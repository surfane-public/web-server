package com.zhpr.portal.web.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhpr.portal.web.service.IConfigService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import java.util.HashMap;

@RestController
@RequestMapping("/system")
public class ConfigController {

    private IConfigService configService;
    private HashMap<String,String> sessionMap = new HashMap<>();

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public  String Test() {
        return configService.Test();
    }

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public  String Login(int id,String data,ServerWebExchange session){
        String result = configService.Config(id,"",data);
        this.loginHandle(session.getRequest().getId(),result);
        return result;
    }

    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public  String Logout(int id,ServerWebExchange session){
        this.logoutHandle(session.getRequest().getId());
        return configService.Config(id,null,null);
    }

    private Boolean loginHandle(String id, String result){
        ObjectMapper objectMapper=new ObjectMapper();
        try
        {
            JsonNode json = objectMapper.readTree(result);
            if(Boolean.valueOf(json.get("flag").toString())){
                sessionMap.put(id,json.get("data").get("uid").toString());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            return true;
        }
    }

    private void logoutHandle(String id){
        sessionMap.remove(id);
    }

    public void setConfigService(IConfigService configService) {
        this.configService = configService;
    }
}
