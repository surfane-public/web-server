package com.zhpr.portal.web.service.impl;

import com.zhpr.portal.web.service.IConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class JdbcService implements IConfigService {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private String sqlCommand;
    private String testCommand;

    @Override
    public String Test(){
        return jdbcTemplate.queryForObject(testCommand,String.class);
    }

    @Override
    public String Config(int id,String userUid,String data){
        String sql = String.format(sqlCommand,id,userUid,data);
        System.out.println("Execute sql command: " + sql);
        System.out.println("result: " + jdbcTemplate.queryForObject(sql,String.class));
        return jdbcTemplate.queryForObject(sql,String.class);
    }

    public void setSqlCommand(String sqlCommand) {
        this.sqlCommand = sqlCommand;
    }

    public void setTestCommand(String testCommand) {
        this.testCommand = testCommand;
    }
}
